﻿#include <iostream>
#include <cmath>

class Vector {
private:
    double x;
    double y;

public:
    Vector(double a, double b) {
        x = a;
        y = b;
    }

    void show() {
        std::cout << "Vector(" << x << ", " << y << ")" << std::endl;
    }

    double length() {
        return std::sqrt(x * x + y * y);
    }
};

int main() {
    Vector v1(3.0, 4.0);
    v1.show();
    std::cout << "Length: " << v1.length() << std::endl;

    return 0;
}